<?php
// $Id$

/**
 * @file
 * Australia Post administration menu items.
 */

/**
 * Australia Post Online Tool settings.
 *
 * Configure which Aus Post services are quoted to customers.
 *
 * @ingroup forms
 * @see uc_admin_settings_validate()
 */

function uc_auspost_pac_admin_settings($form, &$form_state) {
        $domestic_services = _uc_auspost_pac_services();
        $international_services = _uc_auspost_pac_intl_services();
  $form = array();

  $form['uc_auspost_pac_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
        '#default_value' => variable_get('uc_auspost_pac_api_key'),
        '#description' => 'Enter your API key for the Australia Post Postage Assessment Calculator (Register at the <a href="http://auspost.com.au/devcentre/registration.asp">Aus Post Dev Centre</a>).',
    '#size' => 32,
    '#maxlength' => 50,
  );
  $form['uc_auspost_pac_postcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Shop Postcode'),
        '#default_value' => variable_get('uc_auspost_pac_postcode'),
        '#description' => 'Enter the postcode of the location you are shipping from.',
    '#size' => 5,
    '#maxlength' => 6,
  );
  if($domestic_services!=null){
  $form['uc_auspost_pac_services'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Australia Post Services'),
    '#default_value' => variable_get('uc_auspost_pac_services', $domestic_services),
    '#options' => $domestic_services,
    '#description' => t('Select the Australia Post services that are available to customers.'),
  );
  }else{
  $form['uc_auspost_pac_services'] = array(
    '#markup' => '<label>'.t('Australia Post Services').'</label>'.
    '<div class="description">Enter a valid API key to see list of services.</div>',
  );
  }

  if($international_services!=null){
  $form['uc_auspost_pac_intl_services'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Australia Post International Services'),
    '#default_value' => variable_get('uc_auspost_pac_intl_services', $international_services),
    '#options' => $international_services,
    '#description' => t('Select the Australia Post services that are available to customers.'),
  );
  }else{
  $form['uc_auspost_pac_intl_services'] = array(
    '#markup' => '<label>'.t('Australia Post International Services').'</label>'.
    '<div class="description">'.t('Enter a valid API key to see list of services.').'</div>',
  );
  }
  $form['uc_auspost_pac_display_graphics'] = array(
    '#type' => 'radios',
    '#title' => t('Display Service Logos During Checkout?'),
    '#default_value' => variable_get('uc_auspost_pac_display_graphics', 1),
    '#options' => array(
      t('No'),
      t('Yes')
    ),
    '#description' => t('Choice is between displaying text or logos for the various AusPost service types at checkout.')
  );

  $form['uc_auspost_pac_display_time_estimate'] = array(
    '#type' => 'radios',
    '#title' => t('Display Delivery Time Estimate During Checkout?'),
    '#default_value' => variable_get('uc_auspost_pac_display_time_estimate', 1),
    '#options' => array(
      t('No'),
      t('Yes')
    ),
    '#description' => t('Display the DRC\'s postage time estimate')
  );

  $form['uc_auspost_pac_all_in_one'] = array('#type' => 'radios',
    '#title' => t('Product packages'),
    '#default_value' => variable_get('uc_auspost_pac_all_in_one', 1),
    '#options' => array(
      0 => t('Each in its own package'),
      1 => t('All in one'),
    ),
    '#description' => t('Indicate whether each product is quoted as shipping separately or all in one package.'),
  );

  $form['uc_auspost_pac_registered'] = array('#type' => 'radios',
    '#title' => t('Registed Post'),
    '#default_value' => variable_get('uc_auspost_pac_registered', 'AUS_SERVICE_OPTION_STANDARD'),
    '#options' => array(
      'AUS_SERVICE_OPTION_STANDARD' => t('Standard'),
      'AUS_SERVICE_OPTION_REGISTERED_POST' => t('Registed'),
    ),
    '#description' => t('Tells Australia Post that we want to send either a registered or unregistered parcel.'),
	);
  $form['uc_auspost_pac_shipment_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipment Processing Time'),
    '#default_value' =>  variable_get('uc_auspost_pac_shipment_time',   '1'),
    '#description' => t('Shipment processing time (in days). Allows for some leeway to be factored in.') . '<br />' . t('Note that weekends are automatically factored in.'),
  );

  $form['uc_auspost_pac_markup_type'] = array(
    '#type' => 'select',
    '#title' => t('Markup type'),
    '#default_value' => variable_get('uc_auspost_pac_markup_type', 'percentage'),
    '#options' => array(
      'percentage' => t('Percentage (%)'),
      'multiplier' => t('Multiplier (x)'),
      'currency' => t('Addition (!currency)', array('!currency' => variable_get('uc_currency_sign', '$'))),
    ),
  );

  $form['uc_auspost_pac_markup'] = array(
    '#type' => 'textfield',
    '#title' => t('Shipping rate markup'),
    '#default_value' => variable_get('uc_auspost_pac_markup', '0'),
    '#description' => t('Markup shipping rate quote by dollar amount, percentage, or multiplier.'),
  );

  return system_settings_form($form);
}
